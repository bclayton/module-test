package example.modules.person;

public class Person {

  private String firstName;
  private String lastName;
  private int age;

  public Person(String firstName, String lastName, int age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  public String firstName() {
    return firstName;
  }

  public String lastName() {
    return lastName;
  }

  public String name() {
    return firstName + " " + lastName;
  }

  public int age() {
    return age;
  }

  public static void main(String[] args) {
    var person = new Person("Brandon", "Clayton", 32);

    var name = person.name();
    var age = person.age();

    System.out.println(String.format("Name: %s, age: %d", name, age));
  }

}
